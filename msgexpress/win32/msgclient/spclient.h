#ifndef _SPCLIENT_TEST_H_
#define _SPCLIENT_TEST_H_

#include <winsock2.h>
#include <windows.h>
#include <stdio.h>
#include <assert.h>

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <errno.h>
#include <time.h>
#include <string.h>
#include <list>

#include "spthread.hpp"

#include "spporting.hpp"
#include "spbuffer.hpp"
#include "spthreadpool.hpp"
#include <spwin32port.hpp>
#include "protobuf.h"
#include "messageutil.h"
#include "package.h"
#include "msgdecoder.h"

#include "test.pb.h"
#include "msgexpress.h"


struct SP_TestEvent {
	enum { eEventRecv, eEventSend };

	OVERLAPPED mOverlapped;
	WSABUF mWsaBuf;
	int mType;
};

struct SP_TestClient {
	SOCKET mFd;

	SP_TestEvent mRecvEvent;
	SP_TestEvent mSendEvent;

	int mIsStop;
};

struct SP_TestStat {
	int mRecvFail;
	int mWSARecvFail;

	int mSendFail;
	int mWSASendFail;

	int mGQCSFail;
};
int initial(HANDLE * hIocp_out, SP_TestClient** clientList_out, const char * host, const int port);
void closeClients(SP_TestClient* const clientList,const int totalClients);
void on_read( SP_TestClient * client, SP_TestEvent * event );
void close_client( SP_TestClient * client );

#endif